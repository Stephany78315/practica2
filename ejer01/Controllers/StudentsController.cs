﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ejer01.Controllers
{
    [ApiController]
    [Route("/api/students")]
    public class StudentsController
    {

        [HttpGet]
        public List<Student> GetStudents()
        {
            List<Student> nuevo = new List<Student>();
            Student student = new Student();
            student.Name = "Marco";
            nuevo.Add(student);
            student.Name = "Paola";
            nuevo.Add(student);
            student.Name = "Leandro";
            nuevo.Add(student);

            return nuevo;
        }

        [HttpPost]
        public Student CrateStudent([FromBody] string personName)
        {
            return new Student()
            {
                Name = personName
            };
        }

        [HttpPut]
        public Student UpateStudent([FromBody]Student student)
        {
            student.Name = "updated";
            return student;
        }

        [HttpDelete]
        public Student DeleteStudent([FromBody]Student student)
        {
            student.Name = "deleted";
            return student;
        }

    }
}

    


